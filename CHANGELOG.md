# Changelog

Changelog of Puravida Gradle Extension.

## Unreleased
### No issue

**add credentials in CI**


[5d33f650b7d1880](https://gitlab.com/puravida-gradle/check-server/commit/5d33f650b7d1880) jorge *2018-09-13 14:36:45*

**add credentials in CI**


[4e048c5a51a4c72](https://gitlab.com/puravida-gradle/check-server/commit/4e048c5a51a4c72) jorge *2018-09-13 14:32:35*

**add credentials in CI**


[2d383a0d4d39374](https://gitlab.com/puravida-gradle/check-server/commit/2d383a0d4d39374) jorge *2018-09-13 14:27:42*

**we need to find a way to test without send the tweet**


[eca0ae0da44de37](https://gitlab.com/puravida-gradle/check-server/commit/eca0ae0da44de37) jorge *2018-09-13 14:18:13*


## v0.0.1
### No issue

**we need to find a way to test without send the tweet**


[3deb8e3250f9fe2](https://gitlab.com/puravida-gradle/check-server/commit/3deb8e3250f9fe2) jorge *2018-09-13 13:34:00*

**avoid to send a twitter in build CI**


[9de10fa0ee6984a](https://gitlab.com/puravida-gradle/check-server/commit/9de10fa0ee6984a) jorge *2018-09-13 13:30:02*

**improve documentation for twitter**


[0ec8cb79cffba21](https://gitlab.com/puravida-gradle/check-server/commit/0ec8cb79cffba21) jorge *2018-09-13 13:25:17*

**add some usefull plugins**


[70f8c9e6a033093](https://gitlab.com/puravida-gradle/check-server/commit/70f8c9e6a033093) jorge *2018-09-13 11:35:30*


