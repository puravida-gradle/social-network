package com.puravida.gradle.socialnetwork

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class SlackTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.socialnetwork'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {
        File dir = testProjectDir.newFolder("images")
        new File('1.png', dir).bytes = new File('images/1.png').bytes
        new File('2.png', dir).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeTask[]
            slack{
                // we can use project properties
                credentials{    //<1>
                    token = slackToken                                                                                
                    channels = slackChannels
                    service = slackService
                }
                message \"""
                <!here>
                *Testing* a new #puravida #gradle #SocialNetwork 
                Hello & <world> 🌊                
                \"""   //<2>                                                
                media 'images' //<3>                
            }
// end::executeTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PslackToken="+prp["slack_token"],
                "-PslackService="+prp["slack_service"],
                "-PslackChannels="+prp["slack_channels"],
                'slack',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":slack").outcome == SUCCESS
    }

}
