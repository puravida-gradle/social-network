package com.puravida.gradle.socialnetwork

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.FAILED

class MastodonTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.socialnetwork'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {

        new File('1.png', testProjectDir.root).bytes = new File('images/1.png').bytes
        new File('2.png', testProjectDir.root).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeTask[]
            mastodon{
                // we can use project properties
                credentials{    //<1>
                    instanceName = mastodonInstanceName
                    accessToken = mastodonAccessToken
                }
                message "Testing a new #puravida #gradle #SocialNetwork in Mastodon"   //<2>                                                
                media file('1.png')                                         //<3>
                media '2.png', 'https://groovy-lang.gitlab.io/101-scripts/images/menu/basico.png'                
            }
// end::executeTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PmastodonInstanceName="+prp["mastodon.instanceName"],
                        "-PmastodonAccessToken="+prp["mastodon.accessToken"],
                        'mastodon',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":mastodon").outcome == SUCCESS
    }


    def "send a thread"() {

        new File('1.png', testProjectDir.root).bytes = new File('images/1.png').bytes
        new File('2.png', testProjectDir.root).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeTask[]
            mastodon{
                // we can use project properties
                credentials{    //<1>
                    instanceName = mastodonInstanceName
                    accessToken = mastodonAccessToken
                }
                message =["Testing a new #puravida #gradle","#SocialNetwork in Mastodon"]   //<2>                                                
                media file('1.png')                                         //<3>
                media '2.png', 'https://groovy-lang.gitlab.io/101-scripts/images/menu/basico.png'  
                asThread true              
            }
// end::executeTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PmastodonInstanceName="+prp["mastodon.instanceName"],
                        "-PmastodonAccessToken="+prp["mastodon.accessToken"],
                        'mastodon',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":mastodon").outcome == SUCCESS
    }

}
