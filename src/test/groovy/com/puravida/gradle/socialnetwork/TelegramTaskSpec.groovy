package com.puravida.gradle.socialnetwork

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.FAILED

class TelegramTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.socialnetwork'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {

        new File('1.png', testProjectDir.root).bytes = new File('images/1.png').bytes
        new File('2.png', testProjectDir.root).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeTask[]
            telegram{
                // we can use project properties
                credentials{    //<1>
                    token = telegramToken
                    channel = telegramChannel                                                            
                }
                message "Testing a new #puravida #gradle #SocialNetwork "   //<2>                                                
                media file('1.png')                                         //<3>
                media '2.png', 'https://groovy-lang.gitlab.io/101-scripts/images/menu/basico.png'                
            }
// end::executeTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtelegramToken="+prp["telegram_token"],
                "-PtelegramChannel="+prp["telegram_channel"],
                'telegram',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":telegram").outcome == SUCCESS
    }

    def "run task with media dir"() {

        File dir = testProjectDir.newFolder("images")
        new File('1.png', dir).bytes = new File('images/1.png').bytes
        new File('2.png', dir).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeDirTask[]
            telegram{
                // we can use project properties
                credentials{    //<1>
                    token = telegramToken
                    channel = telegramChannel                                                                                                                        
                }
                message "Testing a new #puravida #gradle #SocialNetwork "   //<2>                                                
                media 'images'                                         //<3>                                
            }
// end::executeDirTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtelegramToken="+prp["telegram_token"],
                "-PtelegramChannel="+prp["telegram_channel"],
                'telegram',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":telegram").outcome == SUCCESS
    }

    def "run task with html format"() {

        testProjectDir.newFile('test.html').text = """
        <b>bold</b>, <strong>bold</strong>
        <i>italic</i>, <em>italic</em>
        <a href="http://www.example.com/">inline URL</a>
        <a href="tg://user?id=123456789">inline mention of a user</a>
        <code>inline fixed-width code</code>
        <pre>pre-formatted fixed-width code block</pre>
        """

        buildFile << """

// tag::executeHTMLTask[]
            telegram{
                // we can use project properties
                credentials{    //<1>
                    token = telegramToken
                    channel = telegramChannel                                                                                                                        
                }
                message file('test.html')   //<2>                                                
                sendAsHTML() //<3>                                
            }
// end::executeHTMLTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtelegramToken="+prp["telegram_token"],
                "-PtelegramChannel="+prp["telegram_channel"],
                'telegram',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":telegram").outcome == SUCCESS
    }

    def "run task with markdown format"() {

        testProjectDir.newFile('test.md').text = """
        *bold text*
        _italic text_
        [inline URL](http://www.example.com/)
        [inline mention of a user](tg://user?id=123456789)
        `inline fixed-width code`
        ```block_language
        pre-formatted fixed-width code block
        ```
        """

        buildFile << """

// tag::executeMDTask[]
            telegram{
                // we can use project properties
                credentials{    //<1>
                    token = telegramToken
                    channel = telegramChannel                                                                                                                        
                }
                message file('test.md')  //<2>                                                
                sendAsMarkdown() //<3>                                
            }
// end::executeMDTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtelegramToken="+prp["telegram_token"],
                "-PtelegramChannel="+prp["telegram_channel"],
                'telegram',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":telegram").outcome == SUCCESS
    }


    def "run task with images in html format"() {

        testProjectDir.newFile('test.html').text = """        
        <b>Incredible image bellow</b>
        <a href="https://www.carspecs.us/photos/c8447c97e355f462368178b3518367824a757327-2000.jpg"> ‏ </a>
        """

        buildFile << """

// tag::executeImageInHTMLTask[]
            telegram{
                // we can use project properties
                credentials{    //<1>
                    token = telegramToken
                    channel = telegramChannel                                                                                                                        
                }
                message file('test.html')   //<2>                                                
                sendAsHTML() //<3>                  
                previewMessage false //<4>              
            }
// end::executeImageInHTMLTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('settings.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PtelegramToken="+prp["telegram_token"],
                        "-PtelegramChannel="+prp["telegram_channel"],
                        'telegram',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":telegram").outcome == SUCCESS
    }

}
