package com.puravida.gradle.socialnetwork

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.FAILED

class TwitterTaskSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.socialnetwork'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {

        new File('1.png', testProjectDir.root).bytes = new File('images/1.png').bytes
        new File('2.png', testProjectDir.root).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeTask[]
            twitter{
                // we can use project properties
                credentials{    //<1>
                    consumerKey = twitterConsumerKey
                    consumerSecret = twitterConsumerSecret
                    accessToken = twitterAccessToken
                    accessTokenSecret = twitterAccessTokenSecret                                                            
                }
                message "Testing a new #puravida #gradle #SocialNetwork "   //<2>                                                
                media file('1.png')                                         //<3>
                media '2.png', 'https://groovy-lang.gitlab.io/101-scripts/images/menu/basico.png'                
            }
// end::executeTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtwitterConsumerKey="+prp["oauth.consumerKey"],
                "-PtwitterConsumerSecret="+prp["oauth.consumerSecret"],
                "-PtwitterAccessToken="+prp["oauth.accessToken"],
                "-PtwitterAccessTokenSecret="+prp["oauth.accessTokenSecret"],
                'twitter',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":twitter").outcome == SUCCESS
    }

    def "run task with media dir"() {

        File dir = testProjectDir.newFolder("images")
        new File('1.png', dir).bytes = new File('images/1.png').bytes
        new File('2.png', dir).bytes = new File('images/2.png').bytes

        buildFile << """

// tag::executeDirTask[]
            twitter{
                // we can use project properties
                credentials{    //<1>
                    consumerKey = twitterConsumerKey
                    consumerSecret = twitterConsumerSecret
                    accessToken = twitterAccessToken
                    accessTokenSecret = twitterAccessTokenSecret                                                            
                }
                message "Testing a new #puravida #gradle #SocialNetwork "   //<2>                                                
                media 'images'                                         //<3>                                
            }
// end::executeDirTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                "-PtwitterConsumerKey="+prp["oauth.consumerKey"],
                "-PtwitterConsumerSecret="+prp["oauth.consumerSecret"],
                "-PtwitterAccessToken="+prp["oauth.accessToken"],
                "-PtwitterAccessTokenSecret="+prp["oauth.accessTokenSecret"],
                'twitter',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":twitter").outcome == SUCCESS
    }

    def "send a thread of messages"() {

        buildFile << """

// tag::executeThreadTask[]
            twitter{
                // we can use project properties
                credentials{    //<1>
                    consumerKey = twitterConsumerKey
                    consumerSecret = twitterConsumerSecret
                    accessToken = twitterAccessToken
                    accessTokenSecret = twitterAccessTokenSecret                                                            
                }
                message = [
                    "Testing to create a thread with the new #puravida #gradle #SocialNetwork ",   //<2>
                    "Instead of send a simple String or a File",
                    "now you can set an array of String or files" ]                                                
            }
// end::executeThreadTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PtwitterConsumerKey="+prp["oauth.consumerKey"],
                        "-PtwitterConsumerSecret="+prp["oauth.consumerSecret"],
                        "-PtwitterAccessToken="+prp["oauth.accessToken"],
                        "-PtwitterAccessTokenSecret="+prp["oauth.accessTokenSecret"],
                        'twitter',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":twitter").outcome == SUCCESS
    }

    def "send a mix of string and files in a thread "() {

        File dir = testProjectDir.newFolder("images")
        new File('1.png', dir).bytes = new File('images/1.png').bytes
        new File('2.png', dir).bytes = new File('images/2.png').bytes
        new File('message.txt', dir).text = "𝐭𝐡𝐢𝐬 𝐢𝐬 𝐚 𝐮𝐧𝐢𝐜𝐨𝐝𝐞 𝐬𝐭𝐫𝐢𝐧𝐠 𝐢𝐧 𝐚 𝐟𝐢𝐥𝐞"
        buildFile << """

// tag::executeDirThreadTask[]
            twitter{
                // we can use project properties
                credentials{    //<1>
                    consumerKey = twitterConsumerKey
                    consumerSecret = twitterConsumerSecret
                    accessToken = twitterAccessToken
                    accessTokenSecret = twitterAccessTokenSecret                                                            
                }
                message = [
                    "Testing to create a thread with the new #puravida #gradle #SocialNetwork ",   //<2>
                    "Instead of send a simple String or a File \\n and you can attach 1 image in the tweet",
                    "now you can set an array of String or files",
                    file('images/message.txt')
                     ]
                    
                media 'images'                                         //<3>                                                
            }
// end::executeDirThreadTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PtwitterConsumerKey="+prp["oauth.consumerKey"],
                        "-PtwitterConsumerSecret="+prp["oauth.consumerSecret"],
                        "-PtwitterAccessToken="+prp["oauth.accessToken"],
                        "-PtwitterAccessTokenSecret="+prp["oauth.accessTokenSecret"],
                        'twitter',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":twitter").outcome == SUCCESS
    }

    def "send a mix of string and files independtly"() {

        File dir = testProjectDir.newFolder("images")
        new File('1.png', dir).bytes = new File('images/1.png').bytes
        new File('2.png', dir).bytes = new File('images/2.png').bytes
        new File('message.txt', dir).text = "𝐭𝐡𝐢𝐬 𝐢𝐬 𝐚 𝐮𝐧𝐢𝐜𝐨𝐝𝐞 𝐬𝐭𝐫𝐢𝐧𝐠 𝐢𝐧 𝐚 𝐟𝐢𝐥𝐞"
        buildFile << """

// tag::executeDirNoThreadTask[]
            twitter{
                // we can use project properties
                credentials{    //<1>
                    consumerKey = twitterConsumerKey
                    consumerSecret = twitterConsumerSecret
                    accessToken = twitterAccessToken
                    accessTokenSecret = twitterAccessTokenSecret                                                            
                }
                message = [
                    "Testing to create a thread with the new #puravida #gradle #SocialNetwork ",   //<2>
                    "Instead of send a simple String or a File \\n and you can attach 1 image in the tweet",
                    "now you can set an array of String or files",
                    file('images/message.txt')
                     ]
                    
                media 'images'                                         //<3>
                
                asThread false //4                                                
            }
// end::executeDirNoThreadTask[]
            
        """

        when:
        Properties prp = new Properties()
        prp.load(new File('credentials.properties').newInputStream())

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        "-PtwitterConsumerKey="+prp["oauth.consumerKey"],
                        "-PtwitterConsumerSecret="+prp["oauth.consumerSecret"],
                        "-PtwitterAccessToken="+prp["oauth.accessToken"],
                        "-PtwitterAccessTokenSecret="+prp["oauth.accessTokenSecret"],
                        'twitter',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":twitter").outcome == SUCCESS
    }

}
