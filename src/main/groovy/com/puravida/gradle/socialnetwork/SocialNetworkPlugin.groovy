package com.puravida.gradle.socialnetwork

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.DependencySet

class SocialNetworkPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.with {
            apply(plugin: 'base')

            task('twitter', type:TwitterTask, group:'others',description: 'publish a tweet'){

            }

            task('telegram', type:TelegramTask, group:'others',description: 'send a message to a Telegram channel'){

            }

            task('slack', type:SlackTask, group:'others',description: 'send a message to a Slack room'){

            }

            task('mastodon', type:MastodonTask, group:'others',description: 'send a toot'){

            }
        }
    }

}
