package com.puravida.gradle.socialnetwork.twitter

import twitter4j.StatusUpdate
import twitter4j.Twitter
import twitter4j.TwitterFactory
import twitter4j.UploadedMedia
import twitter4j.auth.AccessToken

class TwitterProxy {


    private Twitter buildTwitter(CredentialDSL credentialDSL){

        Twitter twitter = TwitterFactory.newInstance().instance
        if( credentialDSL?.consumerKey ){
            twitter.setOAuthConsumer(credentialDSL.consumerKey,credentialDSL.consumerSecret)
            twitter.setOAuthAccessToken(new AccessToken(credentialDSL.accessToken, credentialDSL.accessTokenSecret))
        }
        twitter
    }

    long sendTweet( Object message, List<File> medias, CredentialDSL credentialDSL=null){

        Twitter twitter = buildTwitter(credentialDSL)

        def mediasUploaded = medias.collect{ File f->
            twitter.uploadMedia(f.name, f.newInputStream())
        }

        String text = message instanceof File ? (message as File).text : "$message"
        StatusUpdate status = new StatusUpdate(text)
        status.mediaIds = mediasUploaded*.mediaId

        String str = System.properties['PURAVIDA_GRADLE_TWITTER']
        if( System.properties['PURAVIDA_GRADLE_TWITTER']?.equals("false")) {
            println "No se envia el mensaje $text por configuracion"
            return 1L
        }

        twitter.updateStatus(status).id
    }

    void sendThread( List messages, List<File> medias, boolean asThread, CredentialDSL credentialDSL=null){
        Twitter twitter = buildTwitter(credentialDSL)

        long last = -1
        messages.eachWithIndex{ Object message, int i ->

            def attachments = medias.findAll{ it.name.startsWith("${i}.")}
            def mediasUploaded = attachments.collect{ File f->
                twitter.uploadMedia(f.name, f.newInputStream())
            }

            String text = message instanceof File ? (message as File).text : "$message"

            StatusUpdate status = new StatusUpdate(text)
            status.mediaIds = mediasUploaded*.mediaId
            if( i && asThread)
                status.inReplyToStatusId = last

            if( System.properties['PURAVIDA_GRADLE_TWITTER']?.equals("false")) {
                println "No se envia el mensaje $text por configuracion"
                return 1L
            }
            last = twitter.updateStatus(status).id
        }
    }


}
