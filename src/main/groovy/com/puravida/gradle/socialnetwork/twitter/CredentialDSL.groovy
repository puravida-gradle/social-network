package com.puravida.gradle.socialnetwork.twitter

import twitter4j.TwitterFactory

class CredentialDSL {

    String consumerKey
    String consumerSecret
    String accessToken
    String accessTokenSecret


    void consumerKey(String str){
        this.consumerKey=str
    }
    void consumerSecret(String str){
        this.consumerSecret=str
    }
    void accessToken(String str){
        this.accessToken=str
    }
    void accessTokenSecret(String str){
        this.accessTokenSecret=str
    }

}
