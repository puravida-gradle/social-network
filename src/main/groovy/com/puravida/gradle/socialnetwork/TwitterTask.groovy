package com.puravida.gradle.socialnetwork

import com.puravida.gradle.socialnetwork.twitter.CredentialDSL
import com.puravida.gradle.socialnetwork.twitter.TwitterProxy
import groovy.util.logging.Log
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

@Log
class TwitterTask extends DefaultTask{

    private CredentialDSL credentialDSL

    @Optional
    @Input
    Boolean getAsThread(){
        asTreadProp
    }

    boolean asTreadProp = true
    void setAsThread(boolean b){
        this.asTreadProp= b
    }

    void asThread( boolean b){
        this.asTreadProp = b
    }


    @Optional
    @Input
    void credentials( @DelegatesTo(value=CredentialDSL, strategy = Closure.DELEGATE_FIRST) Closure cl){
        credentialDSL = new CredentialDSL()
        Closure clone = cl.rehydrate(credentialDSL, project, this)
        clone()
    }

    @Input
    Object getMessage(){
        return message?.toString()
    }

    Object message

    void setMessage(Object message){
        this.message = message
    }

    void message(Object message){
        this.message = message
    }

    private List<Object>medias=[]

    @Internal
    private boolean validMediaExtension(String url){
        url = url.toLowerCase()
        if( url.endsWith('.png'))
            return true
        if( url.endsWith('.jpg'))
            return true
        false
    }

    @Internal
    List<Object> buildFileList(){
        List<Object>converted=[]
        medias.each{
            def add = [it]
            try {
                File file = project.file(it)
                if (file.isDirectory()) {
                    add = (it as File).listFiles().take(4)
                } else {
                    add = [file]
                }
            }catch(InvalidUserDataException iudae){
                if( it instanceof String){
                    String url = it.toString()
                    if( url.endsWith('.png') || url.endsWith('.jpg')){
                        def tmp = new File(temporaryDir, url.substring(url.lastIndexOf('/')))
                        tmp.bytes = url.toURL().bytes
                        add = [tmp]
                    }
                }
            }
            converted.addAll add
        }
        converted
    }

    @InputFiles
    @Optional
    FileCollection getMedia(){
        List<Object>converted=buildFileList()
        project.files(converted)
    }

    void setMedia(Object ...medias){
        this.medias.clear()
        media(medias)
    }

    void media(Object ...medias){
        this.medias.addAll(medias as List)
    }


    @TaskAction
    void callYourMethodAsYouWant(){
        try {
            assert message, "a message must to be provider"

            if( message instanceof List){
                new TwitterProxy().sendThread(message as List, media.toList(), asThread, credentialDSL)
            }else {
                new TwitterProxy().sendTweet(message, media.toList(), credentialDSL)
            }
        }catch(Exception e){
            log.severe(e.message)
            throw new TaskExecutionException(this,new Exception("Exception occured while processing twitter task",e));
        }
    }


}
