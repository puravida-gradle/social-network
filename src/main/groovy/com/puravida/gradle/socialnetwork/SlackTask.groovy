package com.puravida.gradle.socialnetwork

import com.puravida.gradle.socialnetwork.slack.SlackDSL
import com.puravida.gradle.socialnetwork.slack.SlackApi
import groovy.util.logging.Log
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

@Log
class SlackTask extends DefaultTask{

    private SlackDSL configuration

    @Optional
    @Input
    void credentials( @DelegatesTo(value=SlackDSL, strategy = Closure.DELEGATE_FIRST) Closure cl){
        configuration = new SlackDSL()
        Closure clone = cl.rehydrate(configuration, project, this)
        clone()
    }


    @Input
    Object getMessage(){
        return message
    }

    Object message

    void setMessage(Object message){
        this.message = message
    }

    void message(Object message){
        this.message = message
    }

    private List<Object>medias=[]

    @Internal
    private boolean validMediaExtension(String url){
        url = url.toLowerCase()
        if( url.endsWith('.png'))
            return true
        if( url.endsWith('.jpg'))
            return true
        false
    }

    @Internal
    List<Object> buildFileList(){
        List<Object>converted=[]
        medias.each{
            def add = [it]
            try {
                File file = project.file(it)
                if (file.isDirectory()) {
                    add = (it as File).listFiles()
                } else {
                    add = [file]
                }
            }catch(InvalidUserDataException iudae){
                if( it instanceof String){
                    String url = it.toString()
                    if( url.endsWith('.png') || url.endsWith('.jpg')){
                        def tmp = new File(temporaryDir, url.substring(url.lastIndexOf('/')))
                        tmp.bytes = url.toURL().bytes
                        add = [tmp]
                    }
                }
            }
            converted.addAll add
        }
        converted
    }

    @InputFiles
    @Optional
    FileCollection getMedia(){
        List<Object>converted=buildFileList()
        project.files(converted)
    }

    void setMedia(Object ...medias){
        this.medias.clear()
        media(medias)
    }

    void media(Object ...medias){
        this.medias.addAll(medias as List)
    }


    @TaskAction
    void callYourMethodAsYouWant(){
        try {
            assert message, "a message must to be provider"

            SlackApi api = new SlackApi(
                    token: configuration.token,
                    channels: configuration.channels,
                    service: configuration.service
            )

            String txt
            if( message instanceof File)
                txt = (message as File).text
            else
                txt = "$message"

            api.sendMessage(txt)

            media.toList().each{
               api.sendImage(it as File)
            }

        }catch(Exception e){
            log.severe(e.message)
            throw new TaskExecutionException(this,new Exception("Exception occured while processing twitter task",e));
        }
    }

}
