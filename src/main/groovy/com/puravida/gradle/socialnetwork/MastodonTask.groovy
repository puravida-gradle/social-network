package com.puravida.gradle.socialnetwork

import com.puravida.gradle.socialnetwork.mastodon.MastodonApi
import com.puravida.gradle.socialnetwork.mastodon.MastodonDSL
import groovy.util.logging.Log
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

@Log
class MastodonTask extends DefaultTask{

    private MastodonDSL credentialDSL

    @Optional
    @Input
    void credentials( @DelegatesTo(value= MastodonDSL, strategy = Closure.DELEGATE_FIRST) Closure cl){
        credentialDSL = new MastodonDSL()
        Closure clone = cl.rehydrate(credentialDSL, project, this)
        clone()
    }

    @Input
    Object getMessage(){
        return message?.toString()
    }

    Object message

    void setMessage(Object message){
        this.message = message
    }

    void message(Object message){
        this.message = message
    }

    private List<Object>medias=[]

    @Internal
    private boolean validMediaExtension(String url){
        url = url.toLowerCase()
        if( url.endsWith('.png'))
            return true
        if( url.endsWith('.jpg'))
            return true
        false
    }

    @Internal
    List<Object> buildFileList(){
        List<Object>converted=[]
        medias.each{
            def add = [it]
            try {
                File file = project.file(it)
                if (file.isDirectory()) {
                    add = (it as File).listFiles().take(4)
                } else {
                    add = [file]
                }
            }catch(InvalidUserDataException iudae){
                if( it instanceof String){
                    String url = it.toString()
                    if( url.endsWith('.png') || url.endsWith('.jpg')){
                        def tmp = new File(temporaryDir, url.substring(url.lastIndexOf('/')))
                        tmp.bytes = url.toURL().bytes
                        add = [tmp]
                    }
                }
            }
            converted.addAll add
        }
        converted
    }

    @InputFiles
    @Optional
    FileCollection getMedia(){
        List<Object>converted=buildFileList()
        project.files(converted)
    }

    void setMedia(Object ...medias){
        this.medias.clear()
        media(medias)
    }

    void media(Object ...medias){
        this.medias.addAll(medias as List)
    }

    @Optional
    @Input
    Boolean getAsThread(){
        asTreadProp
    }

    boolean asTreadProp = true
    void setAsThread(boolean b){
        this.asTreadProp= b
    }

    void asThread( boolean b){
        this.asTreadProp = b
    }


    @TaskAction
    void callYourMethodAsYouWant(){
        try {
            assert message, "a message must to be provider"

            if( message instanceof List){
                new MastodonApi(mastodonDSL: credentialDSL).sendThread(message as List, media.toList(), asThread)
            }else {
                new MastodonApi(mastodonDSL: credentialDSL).sendMessage("$message", media.toList())
            }

        }catch(Exception e){
            log.severe(e.message)
            throw new TaskExecutionException(this,new Exception("Exception occured while processing mastodon task",e));
        }
    }
}
