package com.puravida.gradle.socialnetwork.slack

class SlackDSL {

    String token

    String channels

    String service

    void token(String token){
        this.token = token
    }

    void channels(String channels){
        this.channels = channels
    }

    void service(String service){
        this.service = service
    }

}
