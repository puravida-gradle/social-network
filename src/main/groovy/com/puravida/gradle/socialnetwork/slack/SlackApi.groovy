package com.puravida.gradle.socialnetwork.slack

import groovyx.net.http.ApacheEncoders
import groovyx.net.http.NativeHandlers

import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import static groovyx.net.http.MultipartContent.multipart

class SlackApi {

    static final String BASEURL = "https://hooks.slack.com"

    String channels
    String token
    String service

    void sendMessage(String message) {
        configure {
            request.uri = BASEURL
            request.contentType = JSON[0]
        }.post() {
            request.uri.path = "/$service"
            request.body = [
                    text   : message
            ]
        }
    }

    Object sendImage(File file) {
        String extension = file.name.split('\\.').last()
        sendImage(file.name,file,"image/$extension")
    }

    Object sendImage(String name, Object content, String contentType) {
         configure {
            request.uri = 'https://slack.com'
            request.contentType = 'multipart/form-data'
        }.post(){
            request.uri.path = "/api/files.upload"
            request.body = multipart {
                field 'token', token
                field 'channels', channels
                part 'file', name, contentType, content
            }
            request.encoder 'multipart/form-data', ApacheEncoders.&multipart
            request.encoder 'image/png', NativeHandlers.Encoders.&binary
        }
    }

}
