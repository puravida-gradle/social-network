package com.puravida.gradle.socialnetwork.mastodon

class MastodonDSL {

    String instanceName
    String accessToken

    void instanceName(String str){
        this.instanceName = str
    }

    void accessToken(String str){
        this.accessToken=str
    }

}
