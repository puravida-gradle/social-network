package com.puravida.gradle.socialnetwork.mastodon

import com.google.gson.Gson
import com.puravida.gradle.socialnetwork.twitter.CredentialDSL
import com.sys1yagi.mastodon4j.MastodonClient
import com.sys1yagi.mastodon4j.api.method.Media
import com.sys1yagi.mastodon4j.api.method.Statuses
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody

class MastodonApi {

    MastodonDSL mastodonDSL

    long sendMessage(Object message, List<File> medias) {

        MastodonClient client = new MastodonClient.Builder(mastodonDSL.instanceName,
                new OkHttpClient.Builder(), new Gson()).accessToken(mastodonDSL.accessToken).build();

        if( System.properties['PURAVIDA_GRADLE_MASTODON']?.equals("false")) {
            println "No se envia el mensaje $message por configuracion"
            return 1L
        }
        def mediasUploaded = medias.collect{ File f->
            MediaType mediaType = MediaType.parse("image/png")
            byte[]bytes = f.bytes
            new Media(client).postMedia(
                    MultipartBody.Part.createFormData("file", f.name, RequestBody.create( mediaType, bytes) )
            ).execute()
        }
        List<Long> mediasId = mediasUploaded ? mediasUploaded*.id : null
        Statuses statuses = new Statuses(client)
        String text = message instanceof File ? (message as File).text : "$message"
        statuses.postStatus(text,null, mediasId,false,null).execute()
    }

    void sendThread(List messages, List<File> medias, boolean asThread){
        long last = -1
        MastodonClient client = new MastodonClient.Builder(mastodonDSL.instanceName,
                new OkHttpClient.Builder(), new Gson()).accessToken(mastodonDSL.accessToken).build();

        messages.eachWithIndex { Object message, int i ->
            String text = message instanceof File ? (message as File).text : "$message"

            if( System.properties['PURAVIDA_GRADLE_MASTODON']?.equals("false")) {
                println "No se envia el mensaje $text por configuracion"
                return 1L
            }
            def attachments = medias.findAll { it.name.startsWith("${i}.") }
            def mediasUploaded = attachments.collect { File f ->
                MediaType mediaType = MediaType.parse("image/png")
                byte[]bytes = f.bytes
                new Media(client).postMedia(
                        MultipartBody.Part.createFormData("file", f.name, RequestBody.create( mediaType, bytes) )
                ).execute()
            }

            List<Long> mediasId = mediasUploaded ? mediasUploaded*.id : null
            Statuses statuses = new Statuses(client)
            Long inReplyToStatusId=null
            if( i && asThread)
                inReplyToStatusId = last
            last = statuses.postStatus(text,inReplyToStatusId, mediasId,false,null).execute().id
        }
    }
}
