package com.puravida.gradle.socialnetwork.telegram

import groovyx.net.http.ApacheEncoders
import groovyx.net.http.NativeHandlers

import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.MultipartContent.multipart

class TelegramApi {

    static final String BASEURL = "https://api.telegram.org"

    String token

    String channel

    void sendMessage(String message, String format=null, boolean previewMessage=true) {
        configure {
            request.uri = BASEURL
        }.get() {
            request.uri.path = "/bot${token}/sendMessage"
            request.uri.query = [
                    chat_id: channel,
                    text   : message]
            if( format )
                request.uri.query +=[parse_mode: format]
            if( !previewMessage )
                request.uri.query +=[disable_web_page_preview:previewMessage]
        }
    }

    void sendImage(File file) {
        String extension = file.name.split('\\.').last()
        sendImage(file.name,file,"image/$extension")
    }

    void sendImage(String name, Object content, String contentType) {
        configure {
            request.uri = BASEURL
        }.post(){
            request.uri.path = "/bot${token}/sendPhoto"
            request.contentType = 'multipart/form-data'
            request.body = multipart {
                field 'chat_id', channel
                part 'photo', name, contentType, content
            }
            request.encoder 'multipart/form-data', ApacheEncoders.&multipart
            request.encoder 'image/png', NativeHandlers.Encoders.&binary
        }
    }

}
