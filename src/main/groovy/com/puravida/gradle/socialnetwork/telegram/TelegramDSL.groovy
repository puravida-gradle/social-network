package com.puravida.gradle.socialnetwork.telegram

class TelegramDSL {

    String token
    String channel

    void token(String token){
        this.token = token
    }

    void channel(String channel){
        this.channel = channel
    }
}
